import requests, json, re, datetime as dt, os
from functools import reduce
now = dt.datetime.now().strftime('%Y-%m-%d')

class Itunes(object):
    def __init__(self,appid,all=0):
        self.reviews = []
        self.ids = []
        self.appid = str(appid)
        if all==1:
            for i in range(10):
                self.fetchReviews(i)
        else:
            self.fetchReviews(0)
        self.vReviews()
        
    def fetchReviews(self,i):
        page = i+1
        endpoint = "https://itunes.apple.com/us/rss/customerreviews/page=" + str(page) + "/id=" + self.appid + "/sortBy=mostRecent/json"
        self.response = requests.get(endpoint).json()
        for entry in self.response["feed"]["entry"]:
            if "content" in entry:
                self.ids.append(entry["id"]["label"])
                id = entry["id"]["label"]
                name = entry["author"]["name"]["label"]
                content = entry["content"]["label"]
                title = entry["title"]["label"]
                rating = entry["im:rating"]["label"]
                appVersion = entry["im:version"]["label"]
                self.reviews.append({'id':id,
                                     'name':name,
                                     'title':title,
                                     'content':content,
                                     'date':now,
                                     'rating':rating,
                                     'appVersion':appVersion})

    def vReviews(self):
        self.versions = {}
        for review in self.reviews:
            if review['appVersion'] not in self.versions:
                self.versions.update({review['appVersion']:{'ratings':[]}})
            else:
                self.versions[review['appVersion']]['ratings'].append(int(review['rating']))
        for version in self.versions:
            #print(self.versions[version]['ratings'])
            if self.versions[version]['ratings']:
                ratings = self.versions[version]['ratings']
                numRatings = len(ratings)
                avgRating = reduce(lambda x, y: x + y, ratings) / len(ratings)
                avgRating = round(avgRating,2)
                self.versions[version].update({'avgRating':avgRating,'numRatings':numRatings})
            self.versions[version].pop('ratings')
    
    def iStore(self):
        if os.path.exists(self.appid+'.json'):
            data = json.load(open(self.appid+'.json'))
            print('loading to EXISTING database:')
        else:
            data = {'reviews':[],'versions':{},'ids':[]}
            print('loading to a NEW database:')
        oldIds = data['ids']
        newReviews = [r for r in self.reviews if r['id'] not in oldIds]
        newIds = [i for i in self.ids if i not in oldIds]
        data['ids']+=newIds
        data['reviews']+=newReviews
        for v in self.versions:
            if v in data['versions']:
                data['versions'][v].update(self.versions[v])
            else:
                data['versions'].update({v:self.versions[v]})
        with open(self.appid + '.json', 'w') as outfile:
            json.dump(data, outfile)
        print("   '-->",len(newReviews),'new reviews stored in '+self.appid+'.json')

class ItunesStore(object):
    def __init__(self,appid,remoteJson=''):
        self.appid = str(appid)
        if remoteJson:
            self.a = requests.get(remoteJson).json()
            self.__dict__.update(self.a)
        elif os.path.exists(self.appid+'.json'):
            self.a = json.load(open(self.appid+'.json'))
            self.__dict__.update(self.a)
        else:
            error = ('could not find ' + self.appid + '.json \nfirst run:\nItunes(' + self.appid + 
                     ',1).iStore()\nand run store.py on a cron to keep it up to date.')
            print(error)
            self.reviews = []
            self.ids = []
            self.versions = {}